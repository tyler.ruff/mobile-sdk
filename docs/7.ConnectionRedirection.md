# Connection Redirection

With adding more and more connections, its getting hard for user to remember which connections are already established. There is potential that some of the new connections user tries to establish already exists in list of connections (e.g. QR code was scanned multiple types). In this case we should not create new connection and try to reuse existing one.

The high level overview of reusing of an existing connection:

1. Check type of invitation
1. Compare invitation with existing connections
1. Redirect proprietary invitation
1. Reuse Aries invitation

> **NOTE:** library should be initialized before using connections API. See [initialization documentation](2.Initialization.md)

## 1. Check type of invitation

* See [Connection document](3.Connections.md#example-of-a-connection-invitation) to get supported formats.

## 2. Compare invitation with existing connections

To check does connection already exists, we need to retrieve invitations were used during creation of existing connections.

1. Extract details for existing connections

    1. Deserialize connection

        #### iOS
        ```objC
        [appDelegate.sdkApi connectionDeserialize:serializedConnection
                completion:^(NSError *error, NSInteger connectionHandle) {
                    // ...
                }];
        ```
        
        #### Android
        ```java
        int connectionHandle = ConnectionApi.connectionDeserialize(serializedConnection).get();
        ```

    1. Get invite details
    
        #### iOS
        <!--TODO add obj-c sample-->
        
        #### Android
        ```java
        String connectionInvite = ConnectionApi.connectionInviteDetails(handle, abbreviated).get();
        ```
        `abbreviated` parameter takes values `0` or `1`. For `0` it will return full names of invite fields, or shortened names for `1`.

1. Compare `proprietary` invite

    Proprietary invite will have `id` field in the invitation.
    In this case for new invite and connection invite `senderDetail.DID` fields should be compared (`s.d` for abbreviated versions)

1. Compare `aries` invites

    For `aries` invitations should match one of fields: 
    * `@id` field (means exactly the same invitation)
    * `public_did` field (invitation from same Inviter)
    * `recipient_keys[0]` field (invitation from same Inviter)

1. Act base on type of matched invitation 

## 3. Proprietary connection redirection.

In case both invite DIDs are equal, redirection process should  be performed:

1. Deserialize existing Connection state object

    #### iOS
    ```objC
    [appDelegate.sdkApi connectionDeserialize:serializedConnection
            completion:^(NSError *error, NSInteger connectionHandle) {
                // ...
            }];
    ```
    
    #### Android
    ```java
    int existingConnectionHandle = ConnectionApi.connectionDeserialize(serializedConnection).get();
    ```

2. Create Connection state object with new invite

    #### iOS
    ```objC
    [appDelegate.sdkApi connectionCreateWithInvite:invitationId
            inviteDetails:newInvite
            completion:^(NSError *error, NSInteger connectionHandle) {
                // ...
            }];
    ```
    
    #### Android
    ```java
    int newConnectionHandle = ConnectionApi.vcxCreateConnectionWithInvite(invitationId, newInvite).get();
    ```

3. Redirect Connection

    #### iOS
    <!--TODO check new and existing connection param order-->
    ```objC
    [appDelegate.sdkApi connectionRedirect:newConnectionHandle
            withConnectionHandle:existingConnectionHandle
            withCompletion:^(NSError *error) {
                // ...
            }];
    ```
    
    #### Android
    ```java
    int newConnectionHandle = ConnectionApi.vcxConnectionRedirect(newConnectionHandle, existingConnectionHandle).get();
    ```

## 4. Aries connection redirection

Aries RFCs provides [Out-of-Band](https://github.com/hyperledger/aries-rfcs/tree/master/features/0434-outofband) protocol describing the way of creating new connections and reusing of existing.

The steps need to be taken by the app depends on the connection existence and the view of received Out-of-Band invitation.

Bellow we will describes steps for a case when connection matching to the provided invitation is already exists and there is no action attached (`request~attach`) to the invitation.

If you want to get guidelines on how to handle all possible Out-of-Band invitation cases go to the [document](./Out-of-Band.md)

#### 4.1 Aries Connection Reusing 

For this section we assume the following facts:
1. Connection matching to the provided invitation is already exists.
1. Out-of-Band invitation contains `handshake_protocols`
1. Out-of-Band invitation does not `request~attach` or it's an empty array.

1. Deserialize existing Connection state object

    #### iOS
    ```objC
    [appDelegate.sdkApi connectionDeserialize:serializedConnection
            completion:^(NSError *error, NSInteger connectionHandle) {
                // ...
            }];
    ```
    
    #### Android
    ```java
    int existingConnectionHandle = ConnectionApi.connectionDeserialize(serializedConnection).get();
    ```

1. Send Connection Reuse message

    #### iOS
    ```objC
    [appDelegate.sdkApi connectionSendReuse:existingConnectionHandle
            invite:newInvite
            withCompletion:^(NSError *error) {
                // ...
            }];
    ```
    
    #### Android
    ```java
     ConnectionApi.connectionSendReuse(existingConnectionHandle, newInvite).get();
    ```
    
1. Await for `handshake-reuse-accepted` message is received for old Connection. 

   See [messages documentation](8.Messages.md) for message download information.
   Pending messages with `handshake-reuse-accepted` type should be downloaded.
   
   Next, update message status as read. See [messages documentation](8.Messages.md) for message update information.
