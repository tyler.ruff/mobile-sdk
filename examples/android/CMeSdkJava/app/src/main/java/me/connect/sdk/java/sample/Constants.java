package me.connect.sdk.java.sample;

public interface Constants {
    String WALLET_NAME = "wallet-name";
    String PREFS_NAME = "prefs";
    String SPONSEE_ID = "sponsee_id";
    String PROVISION_TOKEN = "provision_token";
    String FCM_TOKEN = "fcm_token";
    String FCM_TOKEN_SENT = "fcm_token_sent";
    String PROVISION_TOKEN_RETRIEVED = "provision_token_retrieved";
    String PLACEHOLDER_SERVER_URL = "placeholder_server_url";
    //URL of your server as a sponsor that would return signed provision token
    String SERVER_URL = "placeholder_server_url"; //enter here your URL instead of placeholder
}
