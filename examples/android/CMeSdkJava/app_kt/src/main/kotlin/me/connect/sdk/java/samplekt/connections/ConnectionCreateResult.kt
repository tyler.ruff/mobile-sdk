package me.connect.sdk.java.samplekt.connections

enum class ConnectionCreateResult {
    SUCCESS,
    REDIRECT,
    FAILURE
}
